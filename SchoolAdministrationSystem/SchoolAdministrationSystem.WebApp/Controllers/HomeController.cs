﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SchoolAdministrationSystem.Domain.Models;
using Microsoft.Owin.Security;
using SchoolAdministrationSystem.WebApp.Infrastructure.Auth;
using Microsoft.AspNet.Identity.Owin;
using System.Security.Claims;
using Microsoft.AspNet.Identity;


namespace SchoolAdministrationSystem.WebApp.Controllers
{
    public class HomeController : Controller
    {

        private IUserManager _userManager;

        public HomeController(IUserManager um)
        {
            _userManager = um;
        }

        /// <summary>
        /// Index form which serves as a welcome page to the user with a link to login
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Login page which validates the users credentials
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(string username, string password, string returnUrl)
        {
            CheckFirstAdmin();
            CheckFirstRegister(username, password);
            SchoolSystemUser user = UserManager.Find(username, password);
            if (user == null)
            {
                
                ModelState.AddModelError("", "Invalid username or password!");
            }
            else
            {
                ClaimsIdentity identity = UserManager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);
                AuthorizationManager.SignOut();
                AuthorizationManager.SignIn(identity);

                return Redirect(returnUrl ?? Url.Action("Main", "Home"));


            }
            return View();
        }

        private void CheckFirstAdmin()
        {
            var admin = _userManager.RetrieveUsers().FirstOrDefault(x => x.Username == "Admin");

            if (admin == null)
            {
                User user = new User { FirstName = "Admin Account", Email = "Admin@system.ca", Username = "Admin", LastName = "Admin" , AssignedRole = Role.Administrator};
                _userManager.AddUser(user);
            }
        }

        public ActionResult Main()
        {
            return View();
        }

        public ActionResult SignOut()
        {
            HttpContext.GetOwinContext().Authentication.SignOut();
            return View("Index");
        }

        private IAuthenticationManager AuthorizationManager
        {
            get { return HttpContext.GetOwinContext().Authentication; }
        }

        private SchoolSystemUserManager UserManager
        {
            get { return HttpContext.GetOwinContext().GetUserManager<SchoolSystemUserManager>(); }
        }

        private void CheckFirstRegister(string username, string password)
        {
            if (UserManager.Users.Count() == 0)
            {
                SchoolSystemUser firstUser = new SchoolSystemUser { UserName = username };

                IdentityResult res = UserManager.Create(firstUser, password);
            }
        }
    }
}