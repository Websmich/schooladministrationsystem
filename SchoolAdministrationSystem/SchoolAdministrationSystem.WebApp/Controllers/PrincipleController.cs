﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SchoolAdministrationSystem.Domain.Models;
using SchoolAdministrationSystem.WebApp.Infrastructure.Auth;
using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using SchoolAdministrationSystem.WebApp.Infrastructure.Models;

namespace SchoolAdministrationSystem.WebApp.Controllers
{
    /// <summary>
    /// The principle controller will serve as the main administration portion of the website. Principles have the highest authority while using the system
    /// </summary>
    [Authorize(Roles = "Administrator")]
    public class PrincipleController : Controller
    {

        private IUserManager _userManager;
        private ICourseManager _coursemanager;

        public PrincipleController(IUserManager userManager, ICourseManager courseManager)
        {
            _userManager = userManager;
            _coursemanager = courseManager;
        }
        
        /// <summary>
        /// The main home page of the principle section of the website that links to other sections of the principle section
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Returns a view with all the users entered in the database
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ViewUsers()
        {
            return View(_userManager.RetrieveUsers());
        }

        /// <summary>
        /// HTTP post method call which will return a filtered list of users 
        /// </summary>
        /// <returns></returns>
        public ActionResult ViewUsers(string name)
        {
            TempData["Response"] = "Showing users with '"+name+"' in their first or last name";
            return View(_userManager.FilterUsers(x => x.FirstName.Contains(name) || x.LastName.Contains(name)));
        }

        /// <summary>
        /// when requested, the data base will clear all traces of the user
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public ActionResult DeleteUser(string name)
        {
            SchoolSystemUser user = UserManager.FindByName(name);
            var teacher = _coursemanager.CourseList.FirstOrDefault(x => x.TeacherForCourse == name);
            
            //check to see if the user is a teacher who is currently teaching a course
            if(teacher == null) {
                //make sure the user is in the table first
                if (user != null)
                {
                    IdentityResult roleres = UserManager.RemoveFromRoles(user.Id);
                    //remove them from role first
                    if (roleres.Succeeded)
                    {
                        IdentityResult userRes = UserManager.Delete(user);
                        //if they were removed from the identity table, remove their information from the entity tables
                        if (userRes.Succeeded)
                        {
                            _userManager.RemoveUser(name);
                            TempData["Response"] = "The user was successfully deleted";
                            return RedirectToAction("ViewUsers");
                        }
                    }
                }
            }
            else
            {
                TempData["Error"] = "Error: Could not delete user, as they are currently teaching a course";
                return RedirectToAction("ViewUsers");
            }
            return RedirectToAction("Index");
        }

        /// <summary>
        /// returns the view which allows the principle to create a user
        /// </summary>
        /// <returns></returns>
        public ActionResult CreateUser()
        {
            return View();
        }

        /// <summary>
        /// Creates the user from the specification provided by the user, and enters their information into the database
        /// </summary>
        /// <param name="model"></param>
        /// <param name="role"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CreateUser(UserModel model, string role)
        {
            //Check to see all the information is entered correctl
            if (ModelState.IsValid)
            {
                SchoolSystemUser user = new SchoolSystemUser { UserName = model.Username };
                IdentityResult res = UserManager.Create(user, model.Password);
                //make sure the user was successfully created and added to the user credentials table
                if (res.Succeeded)
                {
                    //add the user to the assigned role they were chosen
                    UserManager.AddToRole(user.Id, role);
                    //create a user to be stored in the user details table relating to the user
                    User entity = new User
                    {
                        Username = model.Username,
                        FirstName = model.firstname,
                        LastName = model.lastname,
                        Email = model.Email,
                        AssignedRole = (Role)Enum.Parse(typeof(Role), role)
                    };

                    //add the user to the table
                    _userManager.AddUser(entity);
                    TempData["Response"] = "The user was successfully created. Please provide the user with their login details.";
                    return View("Index");
                }
                
                else
                {

                    foreach (string error in res.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                    return View();
                }
            }
            //state is not valid, return the view with the errors present
            else
            {
                return View();
            }
        }

        /// <summary>
        /// Retrieves the username being requested to search, and redirects to the user information page
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public ActionResult SearchUser(string username)
        {
            //find the user in the database
            var user = _userManager.RetrieveUsers().FirstOrDefault(x => x.Username == username);

            //If the user with that username exists, return the view with their information
            if (user != null)
            {
                return RedirectToAction("UserInformation", "User", new { uName = user.Username });
            }
            //if the user with that name does NOT exist, return them back to the list of users
            else
            {
                TempData["Error"] = "The user with the username " + username + " does not exist";
                return RedirectToAction("ViewUsers");
            }
        }

        /// <summary>
        /// Method to return the view to allow the principle to create a course
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult CreateCourse()
        {
            return View();
        }

        /// <summary>
        /// Inserts the course into the database, given it is valid
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CreateCourse(Course course)
        {
            var courseExisting = _coursemanager.CourseList.FirstOrDefault(x => x.CourseCode == course.CourseCode);

            if (ModelState.IsValid && courseExisting == null)
            {
                var teacher = _userManager.RetrieveUsers().FirstOrDefault(x => x.Username == course.TeacherForCourse && x.AssignedRole == Role.Teacher);

                if (teacher != null)
                {
                    TempData["Response"] = "The course has been successfully created";
                    _coursemanager.AddCourse(course);
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["Error"] = "A teacher with that username does not exist";
                    return View();
                }

            }
            else
            {
                if (courseExisting != null)
                {
                    TempData["Error"] = "A course with that code already exists";
                }
                return View();
            }
        }

        /// <summary>
        /// Provides the user manager table use with the Indentity Framework
        /// </summary>
        private SchoolSystemUserManager UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<SchoolSystemUserManager>();
            }
        }
        /// <summary>
        /// Provides the role manager table provided by the Identity Framework
        /// </summary>
        private SchoolSystemRoleManager RoleManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<SchoolSystemRoleManager>();
            }
        }
    }
}