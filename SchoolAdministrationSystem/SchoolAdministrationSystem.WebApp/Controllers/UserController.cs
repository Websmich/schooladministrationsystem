﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SchoolAdministrationSystem.Domain.Models;

namespace SchoolAdministrationSystem.WebApp.Controllers
{
    /// <summary>
    /// The user control's primary task is displaying the information related to the logged in user or user being searched for extra details
    /// </summary>
    [Authorize]
    public class UserController : Controller
    {
        private IUserManager _userManager;
        public UserController(IUserManager userManager)
        {
            _userManager = userManager;
        }

        /// <summary>
        /// Displays a page with the users information on it
        /// </summary>
        /// <param name="uName"></param>
        /// <returns></returns>
        public ActionResult UserInformation(string uName)
        {

            //Check to see if it is a user being searched or not, if it is not being searched, reveal the details of the logged in user
            if (uName == null)
            {
                string username = User.Identity.Name;
                
                //find the user
                User user = _userManager.RetrieveUsers().FirstOrDefault(x => x.Username == username);
                //return the view with the model being sent
                return View(user);
            }
            //the user is being searched, so display that user's information
            else
            {
                //find the user
                User user = _userManager.RetrieveUsers().FirstOrDefault(x => x.Username == uName);
                //return view with the users info
                return View(user);
            }
        }

        public ActionResult SaveDetails(User user)
        {
            _userManager.UpdateUser(user);
            TempData["Response"] = "The details regarding that user have been successfully updated";
            return RedirectToAction("Main", "Home");
        }
    }
}