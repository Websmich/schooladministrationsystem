﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SchoolAdministrationSystem.Domain.Models;


namespace SchoolAdministrationSystem.WebApp.Controllers
{
    [Authorize(Roles = "Teacher, Administrator")]
    public class TeacherController : Controller
    {

        private IUserManager _studentManager;

        private ICourseManager _courseManager;

       
        /// <summary>
        /// accesses useManager and courseManager through binding
        /// </summary>
        /// <param name="repo"></param>
        /// <param name="courseRepo"></param>
        public TeacherController(IUserManager repo, ICourseManager courseRepo)
        {
            _studentManager = repo;
            _courseManager = courseRepo;
        }
        // GET: Teacher
        public ActionResult Index()
        {
            // create test teacher user
            return View();
        }

        /// <summary>
        /// view all users in the database with the role of a student
        /// </summary>
        /// <returns></returns>
        public ActionResult ViewAllStudents()
        {
            ViewBag.courseList = _courseManager.CourseList;
            return View(_studentManager.FilterUsers(u=> u.AssignedRole == Role.Student));
        }

        /// <summary>
        /// view all courses and store the username of the teacher viewing these courses
        /// that way they can only edit and delete courses that they teach
        /// </summary>
        /// <returns></returns>
        public ActionResult ViewAllCourses()
        {
            ViewBag.username = User.Identity.Name;
            ViewBag.auth = _studentManager.RetrieveUsers().FirstOrDefault(x => x.Username == User.Identity.Name).AssignedRole;
            return View(_courseManager.CourseList);
        }

        /// <summary>
        /// get method to display the ability to edit students info
        /// subject as well
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult EditStudent(string userName)
        {
            User user = _studentManager.RetrieveUsers().FirstOrDefault(x => x.Username == userName);
             ViewBag.Courses = _courseManager.CourseList.Select(x => x.CourseCode);
            return View(user);
        }

        /// <summary>
        /// post method for being able to edit a student 
        /// and save the changes to the database
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult EditStudent(User user)
        {
            _studentManager.UpdateUser(user);

            TempData["Response"] = "The student has been successfully editted.";
            return RedirectToAction("ViewAllStudents");
        }

        /// <summary>
        /// view class list for users that are enrolled in
        /// the corresponding course
        /// </summary>
        /// <param name="courseCode"></param>
        /// <returns></returns>
        public ActionResult ViewClassList(string courseCode)
        {

            ViewBag.courseCode = courseCode;
            return View(_studentManager.FilterUsers(u=> u.CourseOneCode == courseCode || u.CourseTwoCode == courseCode || u.CourseThreeCode == courseCode));
        }

        /// <summary>
        /// get method to show view to edit 
        /// a desired course.
        /// if the user is a admin allow them to be able to edit the teacher for a course
        /// </summary>
        /// <param name="courseCode"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult EditCourse (string courseCode)
        {
            Course course = _courseManager.CourseList.FirstOrDefault(c => c.CourseCode == courseCode);
           User user = _studentManager.RetrieveUsers().FirstOrDefault(u=> u.Username == User.Identity.Name);
            ViewBag.userRole = user.AssignedRole;
            return View(course);
        }

        /// <summary>
        /// post method update changes made to course changed by the user
        /// </summary>
        /// <param name="course"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult EditCourse(Course course)
        {
            var teacher = _studentManager.RetrieveUsers().FirstOrDefault(x => x.Username == course.TeacherForCourse);

            if (teacher != null)
            {
                if (teacher.AssignedRole == Role.Teacher)
                {
                    _courseManager.UpdateCourse(course);
                }
                else
                {
                    TempData["Error"] = "The username provided does not belong to a teacher. Please enter a valid teacher username";
                    return RedirectToAction("EditCourse", "Teacher", new { courseCode = course.CourseCode });
                }
            }
            else
            {
                TempData["Error"] = "That teacher does not exist. Please supply the username of an existing teacher";
                return RedirectToAction("EditCourse", "Teacher", new { courseCode = course.CourseCode });
            }
            TempData["Response"] = "The course was updated successfully!";
            return RedirectToAction("ViewAllCourses");
        }

        /// <summary>
        /// delete a course from the database
        /// but before that unenroll all students enrolled in the course
        /// </summary>
        /// <param name="courseCode"></param>
        /// <returns></returns>
        public ActionResult DeleteCourse(string courseCode)
        {
            Course course = _courseManager.CourseList.FirstOrDefault(c => c.CourseCode == courseCode);
            _studentManager.RemoveStudentsFromCourses(course);
            _courseManager.DeleteCourse(course);
            TempData["Response"] = "The course was successfully deleted.";
            return RedirectToAction("ViewAllCourses");
        }

       
    }
}