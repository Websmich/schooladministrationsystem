﻿using System;
using System.Collections.Generic;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Microsoft.AspNet.Identity;
using SchoolAdministrationSystem.WebApp.Infrastructure.Models;

namespace SchoolAdministrationSystem.WebApp.Infrastructure.Auth
{
    public class SchoolSystemIdDbContext : IdentityDbContext<SchoolSystemUser>
    {
        public SchoolSystemIdDbContext() : base("SchoolSystemDbConnection")
        {
            Database.SetInitializer<SchoolSystemIdDbContext>(new DbInit());
            Database.Initialize(true);
        }

        public static SchoolSystemIdDbContext Create()
        {
            return new SchoolSystemIdDbContext();
        }
    }

    public class DbInit : DropCreateDatabaseIfModelChanges<SchoolSystemIdDbContext>
    {
        protected override void Seed(SchoolSystemIdDbContext context)
        {
            SchoolSystemUserManager userManager = new SchoolSystemUserManager(new UserStore<SchoolSystemUser>(context));
            SchoolSystemRoleManager roleManager = new SchoolSystemRoleManager(new RoleStore<UserRole>(context));

            string adminRole = "Administrator";
            string teacherRole = "Teacher";
            string studentRole = "Student";

            SeedRole(roleManager, adminRole);
            SeedRole(roleManager, teacherRole);
            SeedRole(roleManager, studentRole);


            string username = "Admin";
            string pass = "Admin1";

            SchoolSystemUser user = userManager.FindByName(username);

            if (user == null)
            {
                userManager.Create(new SchoolSystemUser { UserName = username }, pass);
                user = userManager.FindByName(username);
            }

            if (!userManager.IsInRole(user.Id, adminRole))
            {
                userManager.AddToRole(user.Id, adminRole);
            }

            

            base.Seed(context);
        }

        private void SeedRole(SchoolSystemRoleManager roleManager, string role)
        {
            if (!roleManager.RoleExists(role))
            {
                roleManager.Create(new UserRole(role));
            }
        }
    }


}