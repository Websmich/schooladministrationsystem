﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using SchoolAdministrationSystem.WebApp.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolAdministrationSystem.WebApp.Infrastructure.Auth
{
    public class SchoolSystemRoleManager : RoleManager<UserRole>, IDisposable
    {
        public SchoolSystemRoleManager(RoleStore<UserRole> store) : base(store)
        {

        }

        public static SchoolSystemRoleManager Create(
             IdentityFactoryOptions<SchoolSystemRoleManager> options, IOwinContext context)
        {
            return new SchoolSystemRoleManager(new RoleStore<UserRole>(context.Get<SchoolSystemIdDbContext>()));
        }


    }
}