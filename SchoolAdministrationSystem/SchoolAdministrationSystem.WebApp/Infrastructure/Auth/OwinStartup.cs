﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.AspNet.Identity;

[assembly: OwinStartup(typeof(SchoolAdministrationSystem.WebApp.Infrastructure.Auth.OwinStartup))]

namespace SchoolAdministrationSystem.WebApp.Infrastructure.Auth
{
    public class OwinStartup
    {
        public void Configuration(IAppBuilder app)
        {
            app.CreatePerOwinContext<SchoolSystemIdDbContext>(SchoolSystemIdDbContext.Create);
            app.CreatePerOwinContext<SchoolSystemUserManager>(SchoolSystemUserManager.Create);
            app.CreatePerOwinContext<SchoolSystemRoleManager>(SchoolSystemRoleManager.Create);

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie, LoginPath = new PathString("/Home/Login")
            });
        }
    }
}
