﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolAdministrationSystem.WebApp.Infrastructure.Auth
{
    public class SchoolSystemUserManager : UserManager<SchoolSystemUser>
    {
        public SchoolSystemUserManager(IUserStore<SchoolSystemUser> store) : base(store)
        {

        }

        public static SchoolSystemUserManager Create(
            IdentityFactoryOptions<SchoolSystemUserManager> options,
            IOwinContext context)
        {
            SchoolSystemIdDbContext dbContext = context.Get<SchoolSystemIdDbContext>();
            SchoolSystemUserManager userManager = new SchoolSystemUserManager(new UserStore<SchoolSystemUser>(dbContext));


            //TODO: password validation 

            return userManager;

        }
    }
}