﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolAdministrationSystem.WebApp.Infrastructure.Models
{
    /// <summary>
    /// The role model object for when users have their role assigned to them using ASP.Net identity
    /// </summary>
    public class UserRole : IdentityRole
    {
        public UserRole() : base() { }

        public UserRole(string name) : base(name) { }
    }
}