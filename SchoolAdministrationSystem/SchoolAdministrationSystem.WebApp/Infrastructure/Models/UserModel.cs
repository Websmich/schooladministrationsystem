﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SchoolAdministrationSystem.WebApp.Infrastructure.Models
{
    /// <summary>
    /// The user model provides as a model for the user to log in with that ASP.Net identity can reconize
    /// </summary>
    public class UserModel
    {
        [Required(ErrorMessage = "Please provide a valid username")]
        public string Username { get; set; }
        [Required(ErrorMessage = "Please provide a valid email")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Please provide a valid password")]
        public string Password { get; set; }
        [Required(ErrorMessage = "Please provide a valid first name")]
        public string firstname { get; set; }
        [Required(ErrorMessage = "Please provide a valid last name")]
        public string lastname { get; set; }

    }
}