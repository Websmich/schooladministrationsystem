﻿using SchoolAdministrationSystem.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolAdministrationSystem.Domain.Persistence
{
    public interface IUserRepository
    {
        IEnumerable<User> UserDatabase { get; }

        void SaveUser(User user);

        void RemoveUser(string name);

        void UpdateUser(User user);
        void RemoveStudentsFromCourses(Course course);
    }
}
