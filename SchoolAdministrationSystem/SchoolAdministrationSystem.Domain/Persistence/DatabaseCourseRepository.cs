﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchoolAdministrationSystem.Domain.Models;

namespace SchoolAdministrationSystem.Domain.Persistence
{
    class DatabaseCourseRepository : ICourseRepository
    {
        private SystemDbContext _dbContext;

        public DatabaseCourseRepository()
        {
            _dbContext = new SystemDbContext();
        }
        public IEnumerable<Course> CourseTable
        {
            get
            {
                return _dbContext.Courses;
            }
        }

        public void AddCourse(Course course)
        {
            _dbContext.Courses.Add(course);
            _dbContext.SaveChanges();
        }

        /// <summary>
        /// find a the course in the database that corresponds to the course 
        /// being passed through and remove it from the table
        /// </summary>
        /// <param name="course"></param>
        public void DeleteCourse(Course course)
        {
            Course deleteCourse = _dbContext.Courses.Find(course.CourseCode);

            _dbContext.Courses.Remove(deleteCourse);

            _dbContext.SaveChanges();
        }

        /// <summary>
        /// find a the course in the database that corresponds to the course 
        /// being passed through and update it's attributes
        /// </summary>
        /// <param name="course"></param>
        public void UpdateCourse(Course course)
        {
           Course courseUpdate = _dbContext.Courses.Find(course.CourseCode);

            courseUpdate.ChangeCourse(course);
            _dbContext.SaveChanges();
        }
    }
}
