﻿using SchoolAdministrationSystem.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolAdministrationSystem.Domain.Persistence
{
    public interface ICourseRepository
    {
        IEnumerable<Course> CourseTable { get;}

        void UpdateCourse(Course course);
           
        void AddCourse(Course course);

        void DeleteCourse(Course course);
    }
}
