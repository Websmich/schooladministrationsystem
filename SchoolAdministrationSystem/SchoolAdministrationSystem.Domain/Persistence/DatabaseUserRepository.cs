﻿using SchoolAdministrationSystem.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolAdministrationSystem.Domain.Persistence
{
    class DatabaseUserRepository : IUserRepository
    {

        private SystemDbContext _dbContext;

        public DatabaseUserRepository()
        {
            _dbContext = new SystemDbContext();
        }

        public IEnumerable<User> UserDatabase
        {
            get
            {
                return _dbContext.Users;
            }
        }

        public void SaveUser (User user)
        {
            //add the user to the table
            _dbContext.Users.Add(user);
            //save changes
            _dbContext.SaveChanges();
        }

        public void RemoveUser(string name)
        {
            //find user from the repository
            User user = _dbContext.Users.FirstOrDefault(x => x.Username == name);
            //remove that user
            _dbContext.Users.Remove(user);
            //save the changes
            _dbContext.SaveChanges();
        }

        /// <summary>
        /// find a the user in the database that corresponds to the user 
        /// being passed through and update the user's attributes
        /// </summary>
        /// <param name="updateUser"></param>
        public void UpdateUser(User updateUser)
        {
            User user = _dbContext.Users.Find(updateUser.Username);

            user.Change(updateUser);

            _dbContext.SaveChanges();
        }

        /// <summary>
        /// if any of the users in the database are enrolled
        /// in the course that is being deleted
        /// unenroll them
        /// </summary>
        /// <param name="course"></param>
        public void RemoveStudentsFromCourses(Course course)
        {
            foreach (User user in this.UserDatabase)
            {

                if(user.CourseOneCode== course.CourseCode || user.CourseTwoCode == course.CourseCode || user.CourseThreeCode == course.CourseCode)
                {
                    user.RemoveCourse(course.CourseCode);
                }

            }

           
            _dbContext.SaveChanges();


        }
    }
}
