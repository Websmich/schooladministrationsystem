﻿using SchoolAdministrationSystem.Domain.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolAdministrationSystem.Domain.Persistence
{
    class SystemDbContext : DbContext
    {
        /// <summary>
        /// connect to SchoolSystemDbConnection database
        /// </summary>
        public SystemDbContext() : base("SchoolSystemDbConnection")
        {

        }

        /// <summary>
        /// connect to Users table
        /// </summary>
        public DbSet<User> Users { get; set; }

        /// <summary>
        /// connect to courses table
        /// </summary>
        public DbSet<Course> Courses { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<SystemDbContext>(null);
        }
    }
}
