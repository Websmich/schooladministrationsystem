﻿
using Ninject.Modules;
using SchoolAdministrationSystem.Domain.Models;
using SchoolAdministrationSystem.Domain.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolAdministrationSystem.Domain
{
    public class DomainFactoryModule : NinjectModule
    {
        public override void Load()
        {
            Bind<ICourseRepository>().To<DatabaseCourseRepository>();
            Bind<ICourseManager>().To<CourseManager>().InSingletonScope();

            Bind<IUserManager>().To<UserManager>().InSingletonScope();

            Bind<IUserRepository>().To<DatabaseUserRepository>().InSingletonScope();
        }
    }
}
