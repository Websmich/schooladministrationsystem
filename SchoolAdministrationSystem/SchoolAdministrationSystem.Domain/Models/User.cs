﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolAdministrationSystem.Domain.Models
{
    public enum Role : byte
    {
        Student = 1,
        Teacher = 2,
        Administrator = 3
    }
    /// <summary>
    /// The User class will serve as a parent class which will contain most of the information a user usually has, such as 
    /// a name, an identification number, and an enumeration for what role they are
    /// </summary>
    public class User
    {
        /// <summary>
        /// User's ID number that is a unique representation of that user. 
        /// - Read only.
        /// </summary>
        [Key]
        public string Username { get; set; }

        /// <summary>
        /// First name of the user
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Last name of the user
        /// </summary>
        public string LastName { get; set; }


        /// <summary>
        /// The role that was specifiy which type of user they are
        /// </summary>
        public Role AssignedRole { get; set; }

        public string Email { get; set; }

        public string CourseOneCode { get; set; }

        public string CourseTwoCode { get; set; }

        public string CourseThreeCode { get; set; }


        public void Change(User user)
        {
            this.Email = user.Email;
            this.FirstName = user.FirstName;
            this.LastName = user.LastName;
            this.CourseOneCode = user.CourseOneCode;
            this.CourseTwoCode = user.CourseTwoCode;
            this.CourseThreeCode = user.CourseThreeCode;
        }

        /// <summary>
        /// if the course code being passed in
        /// is one of the courses the current user is 
        /// enrolled in -> remove them from the course
        /// by making the  field null
        /// </summary>
        /// <param name="courseCode"></param>
        public void RemoveCourse(string courseCode)
        {
            if (this.CourseOneCode == courseCode)
            {
                this.CourseOneCode = null;
            }
            else if (this.CourseTwoCode == courseCode)
            {
                this.CourseTwoCode = null;
            }
            else if (this.CourseThreeCode == courseCode)
            {
                this.CourseThreeCode = null;
            }
        }

    }
}
