﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchoolAdministrationSystem.Domain.Persistence;

namespace SchoolAdministrationSystem.Domain.Models
{
    public class UserManager : IUserManager
    {

        private IUserRepository _repo;

        public UserManager(IUserRepository repo)
        {
            _repo = repo;
        }

        public void AddUser(User user)
        {
            _repo.SaveUser(user);
        }

        public IEnumerable<User> FilterUsers(Func<User, bool> matchUser)
        {
            foreach(User user in _repo.UserDatabase)
            {
                if (matchUser(user))
                {
                    yield return user;
                }
            }
        }

        /// <summary>
        /// remove user from a specific course
        /// </summary>
        /// <param name="course"></param>
        public void RemoveStudentsFromCourses(Course course)
        {
            _repo.RemoveStudentsFromCourses(course);

        }

       
        public void RemoveUser(string id)
        {
            _repo.RemoveUser(id);
        }

        public IEnumerable<User> RetrieveUsers()
        {
            return _repo.UserDatabase;
        }

        /// <summary>
        /// update a user's attributes that can be edited
        /// </summary>
        /// <param name="user"></param>
        public void UpdateUser(User user)
        {
            _repo.UpdateUser(user);
        }
    }
}
