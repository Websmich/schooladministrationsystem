﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolAdministrationSystem.Domain.Models
{
    /// <summary>
    /// Interface designed by Michael Webster
    /// 
    /// The IStudentManager class will define which classes the implemenation of this class will Use. These methods will mainly deal
    /// with logic relating to the users
    /// </summary>
    public interface IUserManager
    {

        IEnumerable<User> RetrieveUsers();

        /// <summary>
        /// Method to add users to the database
        /// </summary>
        /// <param name="user"></param>
        void AddUser(User user);

        void RemoveUser(string id);

        void UpdateUser(User user);

        IEnumerable<User> FilterUsers(Func<User, bool> matchUser);

        void RemoveStudentsFromCourses(Course course);
    }
}
