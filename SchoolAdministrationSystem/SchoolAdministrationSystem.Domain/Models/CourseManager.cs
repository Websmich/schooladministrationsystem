﻿using SchoolAdministrationSystem.Domain.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolAdministrationSystem.Domain.Models
{
    class CourseManager : ICourseManager
    {
        private ICourseRepository _courseRepository;

        public CourseManager(ICourseRepository courseRepo)
        {
            _courseRepository = courseRepo;
        }
        /// <summary>
        /// return course list
        /// </summary>
        public IEnumerable<Course> CourseList
        {
            get
            {
                return _courseRepository.CourseTable;
            }
        }

        /// <summary>
        /// add a course to the database
        /// </summary>
        /// <param name="course"></param>
        public void AddCourse(Course course)
        {
            _courseRepository.AddCourse(course);
        }

        /// <summary>
        /// delete a course from the database
        /// </summary>
        /// <param name="course"></param>
        public void DeleteCourse(Course course)
        {
            _courseRepository.DeleteCourse(course);
        }

       
        /// <summary>
        /// pass a course to be updated with new attributes
        /// </summary>
        /// <param name="course"></param>
        public void UpdateCourse(Course course)
        {
            _courseRepository.UpdateCourse(course);
        }
    }
}
