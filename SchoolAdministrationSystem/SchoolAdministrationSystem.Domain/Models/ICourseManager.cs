﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolAdministrationSystem.Domain.Models
{
   public interface ICourseManager
    {
        IEnumerable<Course> CourseList { get; }

        void UpdateCourse(Course course);

        void AddCourse(Course course);

        void DeleteCourse(Course course);

        
    }
}
