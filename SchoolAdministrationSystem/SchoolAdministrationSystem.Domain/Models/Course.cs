﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolAdministrationSystem.Domain.Models
{
    
    /// <summary>
    /// A course class which models a course that a student can be enrolled in, or teacher that is teaching.
    /// </summary>
    
   
    public class Course
    {

      
        /// <summary>
        /// Course code which will serve as a specific identifier to that course
        /// Ex '30000'
        /// </summary>
        [Key]
        [Display(Name = "Course Code")]
        [Required]
        [StringLength(9, MinimumLength =9, ErrorMessage = "The course code must be 9 characters long" )]
        public string CourseCode { get; set; }
        /// <summary>
        /// The full name of the course
        /// </summary>
        [Display(Name = "Course Name")]
        [Required]
        public string CourseName { get; set; }

        /// <summary>
        /// the teacher for the selected course (username of teacher)
        /// </summary>
        [Display(Name = "Teacher Incharge of Course")]
        [Required]
        public string TeacherForCourse { get; set; }


        /// <summary>
        /// change course name to desired course name
        /// change teacher teaching course to desired teacher
        /// </summary>
        /// <param name="course"></param>
        public void ChangeCourse(Course course)
        {
            this.CourseName = course.CourseName;
            this.TeacherForCourse = course.TeacherForCourse;
        }
    }

    
}
